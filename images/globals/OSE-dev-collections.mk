###############################################################################
# Global OSE Makefile Dev
#
# - Consolidate multiple internal targets into dev-collections
###############################################################################

#
## @Dev: BUILD
###############################################################################

###############################################################################
# dev-build
#
# builds the current and all parent images. calls itself on all parent-levels
#
# @see: dev-re-build
###############################################################################
dev-build:  ## Builds the current and all sub-level images.

dev-build: dev-parent-build dev-self-build

dev-parent-build:
	$(call parent_make,dev-build env=$(env))

dev-self-build: dev-re-build



###############################################################################
# dev-build-env
#
# builds the current and all parent images. calls itself on all parent-levels
# also creates the imagestreams
#
# @see: dev-re-build-env
###############################################################################
dev-build-env: ## Builds the current and all parent images. Creates imagestreams

dev-build-env: dev-parent-build-env dev-self-build-env

dev-parent-build-env:
	$(call parent_make,dev-build-env env=$(env))

dev-self-build-env: dev-re-build-env



###############################################################################
# dev-re-build
#
# builds only the current image level
###############################################################################
dev-re-build: ## Builds only the current image level

dev-re-build: internal-delete-buildconfig internal-create-buildconfig internal-build



###############################################################################
# dev-re-build-env
#
# builds only the current level, also destroys and creates the imagestreams
###############################################################################
dev-re-build-env: ## Builds only the current level; recreates the imagestreams

dev-re-build-env: internal-delete-env-imagestreams internal-delete-buildconfig internal-create-env-imagestreams internal-create-buildconfig internal-build



#
## @Dev: DEPLOY
###############################################################################

###############################################################################
# dev-deploy
#
# deploys the application
#
# @see: dev-re-deploy
###############################################################################
dev-deploy: ## Deploys the application, cleans any existing deployconfig

dev-deploy: dev-re-deploy



###############################################################################
# dev-deploy-env
#
# Recreates all pvs, pvcs, services, routes and secrets, also create the
# the deploymentconfig and deploys the pods
#
# @see: dev-re-deploy-env
###############################################################################
dev-deploy-env: ## Recreates all pvc, pvcs, ... deploys afterwards

dev-deploy-env: dev-delete-deploy-env dev-create-deploy-env internal-deploy



###############################################################################
# dev-create-deploy-env
#
# creates all pvs, pvcs, services, routes, secrets and deployconfig
###############################################################################
dev-create-deploy-env: ## Creates all env, thats needed for deployment

dev-create-deploy-env: internal-create-env-pvs internal-create-env-pvcs internal-create-env-services internal-create-env-routes internal-create-env-secrets internal-create-deployconfig



###############################################################################
# dev-delete-deploy-env
#
# deletes all pvs, pvcs, services, routes and secrets, also deletes the
# the deploymentconfig
###############################################################################
dev-delete-deploy-env: ## Deletes all pvs, pvcs, ...

dev-delete-deploy-env: internal-delete-env-pvs internal-delete-env-pvcs internal-delete-env-services internal-delete-env-routes internal-delete-env-secrets internal-delete-deployconfig



###############################################################################
# dev-re-deploy
#
# deploys the application, cleans any previously created deploymentconfigs
#
# @see: dev-delete-deploy-env 
# @see: dev-create-deploy-env 
# @see: internal-deploy
###############################################################################
dev-re-deploy: ## Deploys the application, cleans any existing deployconfigs

dev-re-deploy: dev-delete-deploy-env dev-create-deploy-env internal-deploy



###############################################################################
# dev-re-deploy-env
#
# deletes and recreates all pvs, pvcs, services, routes and secrets, 
# also deletes and recreate the deploymentconfig and deploys the pods
#
# @see: dev-deploy-env
###############################################################################
dev-re-deploy-env: ## Recreates all pvs, pvcs... deploys afterwards

dev-re-deploy-env:  dev-delete-deploy-env dev-create-deploy-env internal-deploy



#
## @Dev: INSTALL
###############################################################################

###############################################################################
# dev-install
#
# builds and deploys the image / pods
#
# @see: dev-build
# @see: dev-deploy
###############################################################################
dev-install: ## Builds and deploys

dev-install: dev-build dev-deploy



###############################################################################
# dev-install-env
#
# builds and deploys the image / pods, also creates all environmentals
#
# @see: dev-build-env
# @see: dev-deploy-env
###############################################################################
dev-install-env: ## Builds and deploys, creates env

dev-install-env: dev-build-env dev-deploy-env



###############################################################################
# dev-re-install
#
# builds and deploys the current image level
#
# @see: dev-re-build
# @see: dev-re-deploy
###############################################################################
dev-re-install: ## Builds and deploys the current level

dev-re-install: dev-re-build dev-re-deploy



###############################################################################
# dev-re-install-env
#
# builds and deploys the current image level, also creates all environments
#
# @see: dev-re-build-env
# @see: dev-re-deploy-env
###############################################################################
dev-re-install-env: ## Builds and deploys current level; creates env

dev-re-install-env: dev-re-build-env dev-re-deploy-env



#
## @Dev: ENVIRONMENT
###############################################################################

###############################################################################
# dev-create-env
#
# creates all pvs, pvcs, services, routes, secrets, builconfig, deployconfig
###############################################################################
dev-create-env: ## Creates alls build- and deploy-environmentals

dev-create-env: internal-create-buildconfig dev-create-deploy-env

###############################################################################
# dev-delete-env
#
# deletes all pvs, pvcs, services, routes, secrets, deployconfig and the buildconfig
#
# @see: dev-delete-deploy-env
###############################################################################
dev-delete-env: ## Removes every environmental available
	
dev-delete-env: internal-delete-buildconfig dev-delete-deploy-env
