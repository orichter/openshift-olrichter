###############################################################################
# Global OSE Makefile Targets
#
# - Translate environment vars to OSE environment variables
# - Provides default targets for the OSE-builds and -deployments
###############################################################################

#
# ENVIRONMENT
###############################################################################

# create the names for the step target definitions of the 
# create and delete environmentals
INTERNAL_ENV_CREATE_TARGETS := $(addsuffix s-step,\
                               $(addprefix internal-create-env-,pv pvc service route imagestream))
INTERNAL_ENV_DELETE_TARGETS := $(addsuffix s-step,\
                               $(addprefix internal-delete-env-,pv pvc service route imagestream))

# define the VAR , as it is needed in function calls
, := ,


#
# TARGETS: BUILD
###############################################################################

###############################################################################
# internal-create-buildconfig
#
# creates the buildconfig using the build template
###############################################################################
internal-create-buildconfig: internal-pre-create-buildconfig internal-create-buildconfig-step internal-post-create-buildconfig

internal-pre-create-buildconfig::

internal-post-create-buildconfig::

internal-create-buildconfig-step:
	$(call yml_oc_importer,build)



###############################################################################
# internal-delete-buildconfig
#
# deletes the buildconfig, ignores any 'not found's
###############################################################################
internal-delete-buildconfig: internal-pre-delete-buildconfig internal-delete-buildconfig-step internal-post-delete-buildconfig

internal-pre-delete-buildconfig::

internal-post-delete-buildconfig::

internal-delete-buildconfig-step:
	@$(call headline,"Deleting all Builds and the BuildConfig for $(APP_NAME)")
	@$(call echoed_call,oc delete bc$(,)build -l app=$(APP_NAME) --ignore-not-found=true)



###############################################################################
# internal-build
#
# builds the image using the buildconfig
###############################################################################
internal-build: internal-pre-build internal-build-step internal-post-build

internal-pre-build::

internal-post-build::

internal-build-step:
	@$(call headline,"Starting the $(APP_NAME) Build")
	@$(call echoed_call,oc start-build $(APP_NAME) --follow --wait)


#
# TARGETS: DEPLOYMENT
###############################################################################

###############################################################################
# internal-create-deployconfig
#
# creates the deploymentconfig using the deploy template
###############################################################################
internal-create-deployconfig: internal-pre-create-deployconfig internal-create-deployconfig-step internal-post-create-deployconfig

internal-pre-create-deployconfig::

internal-post-create-deployconfig::

internal-create-deployconfig-step:
	$(call yml_oc_importer,deploy)



###############################################################################
# internal-delete-deployconfig
#
# deletes the deploymentconfig, ignores any 'not found's
###############################################################################
internal-delete-deployconfig: internal-pre-delete-deployconfig internal-delete-deployconfig-step internal-post-delete-deployconfig

internal-pre-delete-deployconfig::

internal-post-delete-deployconfig::

internal-delete-deployconfig-step:
	@$(call headline,"Deleting the DeploymentConfig for $(APP_NAME)")
	@$(call echoed_call,oc delete dc$(,)rc -l app=$(APP_NAME) --ignore-not-found=true)



###############################################################################
# internal-deploy
#
# deploy the pods using the deploymentconfig
###############################################################################
internal-deploy: internal-pre-deploy internal-deploy-step internal-post-deploy

internal-pre-deploy::

internal-post-deploy::

internal-deploy-step:
	@$(call headline,"Starting the $(APP_NAME) Deployment")
	@$(call echoed_call,oc deploy $(APP_NAME) --latest)


#
# TARGETS: ENVIRONMENT
###############################################################################

###############################################################################
# internal-create-env-pvs
#
# create all the pvs for the image
###############################################################################
internal-create-env-pvs: internal-pre-create-env-pvs internal-create-env-pvs-step internal-post-create-env-pvs

internal-pre-create-env-pvs::

internal-post-create-env-pvs::



###############################################################################
# internal-create-env-pvcs
#
# create all the pvcs for the image
###############################################################################
internal-create-env-pvcs: internal-pre-create-env-pvcs internal-create-env-pvcs-step internal-post-create-env-pvcs

internal-pre-create-env-pvcs::

internal-post-create-env-pvcs::



###############################################################################
# internal-create-env-secrets
#
# create all the secrets for the image
###############################################################################
internal-create-env-secrets: internal-pre-create-env-secrets internal-create-env-secrets-step internal-post-create-env-secrets

internal-pre-create-env-secrets::

internal-post-create-env-secrets::

internal-create-env-secrets-step:
	@true



###############################################################################
# internal-create-env-services
#
# create all the services for the image
###############################################################################
internal-create-env-services: internal-pre-create-env-services internal-create-env-services-step internal-post-create-env-services

internal-pre-create-env-services::

internal-post-create-env-services::



###############################################################################
# internal-create-env-routes
#
# create all the routes for the image
###############################################################################
internal-create-env-routes: internal-pre-create-env-routes internal-create-env-routes-step internal-post-create-env-routes

internal-pre-create-env-routes::

internal-post-create-env-routes::



###############################################################################
# internal-create-env-imagestreams
#
# create all the imagestreams for the image
###############################################################################
internal-create-env-imagestreams: internal-pre-create-env-imagestreams internal-create-env-imagestreams-step internal-post-create-env-imagestreams

internal-pre-create-env-imagestreams::

internal-post-create-env-imagestreams::



###############################################################################
# internal-delete-env-pvs
#
# deletes all the pvs of the image
###############################################################################
internal-delete-env-pvs: internal-pre-delete-env-pvs internal-delete-env-pvs-step internal-post-delete-env-pvs

internal-pre-delete-env-pvs::

internal-post-delete-env-pvs::



###############################################################################
# internal-delete-env-pvcs
#
# deletes all the pvcs of the image
###############################################################################
internal-delete-env-pvcs: internal-pre-delete-env-pvcs internal-delete-env-pvcs-step internal-post-delete-env-pvcs

internal-pre-delete-env-pvcs::

internal-post-delete-env-pvcs::



###############################################################################
# internal-delete-env-secrets
#
# deletes all the secrets of the image
###############################################################################
internal-delete-env-secrets: internal-pre-delete-env-secrets internal-delete-env-secrets-step internal-post-delete-env-secrets

internal-pre-delete-env-secrets::

internal-post-delete-env-secrets::

internal-delete-env-secrets-step:
	@true



###############################################################################
# internal-delete-env-services
#
# deletes all the services of the image
###############################################################################
internal-delete-env-services: internal-pre-delete-env-services internal-delete-env-services-step internal-post-delete-env-services

internal-pre-delete-env-services::

internal-post-delete-env-services::



###############################################################################
# internal-delete-env-routes
#
# deletes all the routes of the image
###############################################################################
internal-delete-env-routes: internal-pre-delete-env-routes internal-delete-env-routes-step internal-post-delete-env-routes

internal-pre-delete-env-routes::

internal-post-delete-env-routes::



###############################################################################
# internal-delete-env-imagestreams
#
# deletes all the imagestreams of the image
###############################################################################
internal-delete-env-imagestreams: internal-pre-delete-env-imagestreams internal-delete-env-imagestreams-step internal-post-delete-env-imagestreams

internal-pre-delete-env-imagestreams::

internal-post-delete-env-imagestreams::



# Auto-generated env creation
$(INTERNAL_ENV_CREATE_TARGETS):
	$(eval OBJECT_TYPE := $(subst internal-create-env-,,$(subst s-step,,$@)))
	$(call object_oc_importer,$(OBJECT_TYPE))
	$(call yml_oc_importer,$(OBJECT_TYPE))

# Auto-generated env deletion
$(INTERNAL_ENV_DELETE_TARGETS):
	$(eval OBJECT_TYPE := $(subst internal-delete-env-,,$(subst s-step,,$@)))
	$(call object_oc_remover,$(OBJECT_TYPE))