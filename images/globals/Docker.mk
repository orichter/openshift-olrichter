###############################################################################
# Global Docker Makefile Targets
#
# - Translates environment vars to OSE environment variables
# - Includes additional environment variables if applicable
# - Provides default targets for the Docker-builds and -runs
###############################################################################

#
# ENVIRONMENT
###############################################################################

# include the local (meaning image specific) env-docker variables (if available),
# if the make target is build, run or rund
ifeq ($(MAKECMDGOALS),build)
	ifneq ("$(wildcard vars-docker)","")
		include ./vars-docker
	endif
endif

ifeq ($(MAKECMDGOALS),run)
	ifneq ("$(wildcard vars-docker)","")
		include ./vars-docker
	endif
endif

ifeq ($(MAKECMDGOALS),rund)
	ifneq ("$(wildcard vars-docker)","")
		include ./vars-docker
	endif
endif

#
# TARGETS
###############################################################################

###############################################################################
# build
#
# builds the image with S2I or Docker, depending on weather a Dockerfile exists, 
# includes all variables prefixed with d_build_ and passes them to 
# the s2i or docker process; builds all levels
###############################################################################

build: pre-build build-parent-step build-step post-build

pre-build::

post-build::

build-parent-step:
	$(call parent_make,build env=$(env))

build-step:
ifeq ($(shell test -e ./Dockerfile && echo -n yes),yes)
	$(call docker_build)
else
	$(call s2i_build)
endif



###############################################################################
# re-build
#
# builds the image with S2I or Docker, depending on weather a Dockerfile exists, 
# includes all variables prefixed with d_build_ and passes them to 
# the s2i or docker process; only builds the current level
###############################################################################

re-build: pre-re-build build-step post-re-build

pre-re-build::

post-re-build::
	


###############################################################################
# run
#
# runs the image in the foreground with docker, includes all variables
# prefixed with d_run and passes them to the docker run process
###############################################################################
run: pre-run run-step post-run

# only exists for consistency
pre-run::

# only exists for consistency
post-run::

run-step:
	$(call docker_run)



###############################################################################
# rund
#
# runs the image in the background with docker, includes all variables
# prefixed with d_run and passes them to the docker run process
###############################################################################
rund: pre-rund rund-step post-rund

rund: DOCKER_RUN_FLAGS = $(DOCKER_DAEMON_RUN_FLAGS)

pre-rund::

post-rund::

rund-step:
	$(call docker_run)



###############################################################################
# attach
#
# attaches to the docker container, that was startet in the 
# background with make rund
###############################################################################
attach: pre-attach attach-step post-attach

# only exists for consistency
pre-attach::

# only exists for consistency
post-attach::

attach-step:
	docker exec -i -t $(IMAGE_NAME) /bin/bash



###############################################################################
# killd
#
# stops and deletes the docker container, that was started in the background
# with make rund
###############################################################################
killd: pre-killd killd-step post-killd

# only exists for consistency
pre-killd::

# only exists for consistency
post-killd::

killd-step:
	docker rm -f $(IMAGE_NAME)



###############################################################################
# rmi
#
# removes the docker image, that was created with make build
###############################################################################
rmi: pre-rmi rmi-step post-rmi

pre-rmi::

post-rmi::

rmi-step:
	docker rmi -f $(IMAGE_NAME)
