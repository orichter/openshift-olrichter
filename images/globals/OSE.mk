###############################################################################
# Global OSE Makefile Loader
#
# - Loads the internals and collections with the OSE Makefile Targets
###############################################################################
include ../globals/OSE-internals.mk
include ../globals/OSE-dev-collections.mk
