# include default environment variables
include ../globals/constants
include ../globals/defaults
include ../globals/defaults-bindings

# include optional image level environment variables (only if available)
ifneq ("$(wildcard vars)","")
	include vars
endif
ifneq ("$(wildcard vars-bindings)","")
	include vars-bindings
endif


# include basic functions, to use in the default targets
include ../utilities/Functions.mk

# include default targets
include ../globals/Docker.mk
include ../globals/OSE.mk

# include the usage tool
include ../utilities/Usage.mk
