## @Misc

help: ## Show this help dialog
	@IFS=$$'\n' ; \
    help_lines=(`fgrep -h "## " $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//'`); \
    echo ">> Portal OSE Makfile Help:"; \
    for help_line in $${help_lines[@]}; do \
        IFS=$$'#' ; \
        help_split=($$help_line) ; \
        help_command=`echo $${help_split[0]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
        help_info=`echo $${help_split[2]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
        if [[ "$$help_info" =~ ^@.* ]]; then\
        help_info="$${help_info} ";\
        printf "\n%s\n" $${help_info:1:-1};\
        echo "---------------------------------------------------------------";\
        else\
        printf "  %-30s %s\n" $$help_command $$help_info ; \
        fi\
    done; \
    echo ""