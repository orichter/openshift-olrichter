#################################################################################
# collects all template yml files
#
# @params: - Object Class: e.g. pv, pvc, etc..
#################################################################################
yml_template_files = $(wildcard *-$1-template.yml)



#################################################################################
# collects all object yml files
#
# @params: - Object Class: e.g. pv, pvc, etc..
#################################################################################
yml_object_files = $(wildcard *-$1.yml)



#################################################################################
# converts a string to it's uppercase version
#
# @params: - any String
#################################################################################
uc = $(shell echo $(1) | tr a-z A-Z)



#################################################################################
# generates the oc clients parameters, that are prefixed with
# the object classes name (e.g. pv_, pvc_ or imagestream_)
#
# @params: - Object Class: e.g. pv, pvc, etc..
#################################################################################
oc_params = $(foreach param,\
            $(filter $1_%,$(.VARIABLES)),\
            -v $(subst $1_,,$(param))=$($(param)))



#################################################################################
# processes and creates the objects of a file, that is matched
# by the yml_template_files wildcard.
# Uses the oc_params function, to pass all prefixed variables
#
# @params: - Object Class: e.g. pv, pvc, etc..
#
# @see: yml_template_files
# @see: oc_create
#################################################################################
yml_oc_importer = $(foreach V,\
                  $(call yml_template_files,$1),\
                  @$(call headline,"Creating $(call uc,$1)s from $(V)"); \
				  $(call echoed_call,oc process -f $V $(call oc_params,$1) | oc create -f -) || true;)



#################################################################################
# creates the objects from an object file, that is matched
# by the yml_object_files wildcard.
# Uses the oc_params function, to pass all prefixed variables
#
# @params: - Object Class: e.g. pv, pvc, etc..
#
# @see: yml_object_files
# @see: oc_create
#################################################################################
object_oc_importer = $(foreach V,\
                     $(call yml_object_files,$1),\
                     @$(call headline,"Creating $(call uc,$1)s from $(V)"); \
				     $(call echoed_call,$(call oc_create,$(V)) || true);)



#################################################################################
# removes all objects of the specified type, that are labeld with the current App
#
# @params: - Object Class: e.g. pv, pvc, etc..
#################################################################################
object_oc_remover = @$(call headline,"Removing $(call uc,$1)s on $(APP_NAME)");\
                    $(call echoed_call,\
                    oc delete $1 -l app=$(APP_NAME) --ignore-not-found=true)



#################################################################################
# passes the contents of an object (file) to the oc create
#
# @params: - Filename (of an object file) or String containing an object definition
#################################################################################
oc_create = (cat $1 | oc create -f -)



#################################################################################
# calls the parent image if its folder exists
#
# @params: - Parents Makefile Call in a seperate shell call, to 
# 			 separate it's environment
#################################################################################
parent_make = $(if $(realpath ../$(BASE_IMAGE_NAME)),\
              @($(call headline,"Building Parent: $(BASE_IMAGE_NAME)");\
              (/bin/bash -c "cd ../$(BASE_IMAGE_NAME) && make $1")),\
              $(info reached deepest image level.))



#################################################################################
# Prints out the command befire executing it
#
# @params: - command to be executed and echoed
#################################################################################
echoed_call = echo "----> $1"; $1



#################################################################################
# Prints out a headline, with an empty line above as a separator
#
# @params: - Headline Text
#################################################################################
headline = echo ""; echo "--> $1..."



#################################################################################
# Prints out an image headline, with an empty line above as a separator
#
# @params: - Headline Text
#################################################################################
image_headline = \
    echo '\#-------------------------------------------------------------';\
    echo "\# $1..."; \
    echo '\#-------------------------------------------------------------';\
    echo ""



#################################################################################
# Builds the Docker Image using the s2i Tool. It includes the image and 
# base image names, as well as the s2i path; it uses the docker_param function
# to get all the environment vars prefixed with d_build_
#
# @see: docker_param
#################################################################################
s2i_build = @$(call headline,"Building $(APP_NAME) with Docker"); \
            $(call echoed_call,(s2i build . $(BASE_IMAGE_NAME) $(IMAGE_NAME) \
            $(call docker_param,d_build) -s $(S2I_PATH) \
             --copy -p if-not-present))



#################################################################################
# Builds the Docker Image using Docker. As the used Docker Version does
# not support arguments, the s2i_params function can not be used in this
# function. All Variables needed must be provided through defaulted variable
# assignments inside the Dockerfile.
#################################################################################
docker_build = @$(call headline,"Building $(APP_NAME) with Docker"); \
			   $(call echoed_call,docker build -t $(APP_NAME) .)



#################################################################################
# Runs the Docker Image as a container. It provides run time variables through
# the docker_param function. It uses all Variables prefixed with d_run_.
#
# @see: docker_param
#################################################################################
docker_run = @$(call headline,"Running $(APP_NAME)"); \
			 $(call echoed_call,(docker run $(call docker_param,d_run) --name=$(IMAGE_NAME) \
             $(DOCKER_RUN_FLAGS) $(DOCKER_SERVICES) -u $(DOCKER_GUID) \
             $(DOCKER_PORTS) $(DOCKER_VOLUMES) -t $(IMAGE_NAME)))



#################################################################################
# generates the s2i parameters, that are prefixed with
# the target shortcode name (e.g. d_build or d_run)
#
# @params: - Target Shortcode Name: e.g. d_build or d_run
#################################################################################
docker_param = $(foreach param,\
               $(filter $1_%,$(.VARIABLES)),\
               -e $(subst $1_,,$(param))=$($(param)))



#################################################################################
# generates the s2i parameters, that are prefixed with
# the target shortcode name (e.g. d_build or d_run)
#
# @params: - Target Shortcode Name: e.g. d_build or d_run
#################################################################################
run_make_target_on_images = $(foreach image, $(INSTALL_IMAGES), $(call image_headline,"Processing on $(image)"); $(call echoed_call,make -C ./$(image) $@ env=$(env);))