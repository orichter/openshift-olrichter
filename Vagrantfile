# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

# Constants
## Public IP address of the vm to connect with openshift 
   PUBLIC_IP_ADDRESS="10.2.2.2"
## Imageversion for efk-stack images
   EFK_STACK_IMAGEVERSION="3.1.1"  

Vagrant.configure(2) do |config|
 
# Automatic registering box with vagrant-registration 
 if Vagrant.has_plugin?('vagrant-registration')
    config.registration.username = 'olrichter'
    config.registration.password = 'redhatolrichter@th-wildau.de'
 end
  
  # Set config option for virtual maschine (vm)
  config.vm.box = "cdkv2"
  config.vm.box_check_update = false  
  config.vm.network "private_network", ip: "#{PUBLIC_IP_ADDRESS}"
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.hostname = "RHEL-CDKv2"
  config.vm.synced_folder  "./data", "/opt/OpenShift/data"
  config.vm.synced_folder  "./images", "/opt/OpenShift/images"  
  
  # Set settings for VirtualBox:
   config.vm.provider "virtualbox" do |vb|
      vb.memory = "4096"
      vb.cpus = 2
   end
   
   # Don't login ssh as root
   config.ssh.username = 'vagrant'
   config.ssh.password = 'vagrant'
   config.ssh.insert_key = 'true'
   
   # Start OpenShift and login as openshift-dev /oc whoami = openshift-dev in existing project default
   config.vm.provision "shell", name: "Start OpenShift", run: 'always', privileged: true, inline: <<-SHELL
    systemctl start openshift
    sleep 15s
    oc login 127.0.0.1:8443 -u admin -p admin --insecure-skip-tls-verify=true    
   SHELL
   
   # Login to OpenShift as admin /oc whoami = admin in existing project default
   config.vm.provision "shell", name: "Login to OpenShift as admin", run: "always", privileged: false, inline: <<-SHELL
    oc login 127.0.0.1:8443 -u admin -p admin --insecure-skip-tls-verify=true    
   SHELL
        
   # Check if project plattform-applikation ist create and ready for work
   config.vm.provision "shell", name: "Check project plattform-applikation exist", run: "always", privileged: false, inline: <<-SHELL
   	if oc new-project plattform-applikation; then 
   		echo "Project plattform-applikation is create"
   	else 
    	echo "Project plattform-applikation exists, no problem" 
    fi
   SHELL
   
   # Check if project nexus is create and ready for work 
   config.vm.provision "shell", name: "Install nexus", run: "always", privileged: false, inline: <<-SHELL      
    if oc new-project nexus; then 
    	echo "Project nexus is create"
    	echo "Login in project nexus"
    	echo "Create the nexus pod"
    	oc new-app sonatype/nexus    	
    	echo "nexus pod create"
    	echo "Create route for nexus pod"
    	oc expose svc/nexus --path='/nexus'
    	oc project plattform-applikation 	 
    else 
    	echo "Project nexus exists, no problem" 
    fi
    SHELL
   
    # Check if project logging is create for efk-stack
     config.vm.provision "shell", name: "Install EFK-Stack", run: "always", privileged: false, inline: <<-SHELL      
    if oc new-project logging; then 
    	echo "Project logging is create"
    	echo "Login in project logging"
    	oc secrets new logging-deployer nothing=/dev/null  
    	oc create -f /opt/OpenShift/data/logging-deployer/serviceaccount-logging-deployer.yaml	
    	echo "serviceaccount logging-deployer create"
    	oc policy add-role-to-user edit system:serviceaccount:logging:logging-deployer
    	oadm policy add-scc-to-user  \
   			 privileged system:serviceaccount:logging:aggregated-logging-fluentd
   		oadm policy add-cluster-role-to-user cluster-reader \
             system:serviceaccount:logging:aggregated-logging-fluentd
        oc create -f /opt/OpenShift/data/logging-deployer/logging-deployer.yaml
        oc new-app logging-deployer-template \
		--param KIBANA_HOSTNAME=kibana.#{PUBLIC_IP_ADDRESS}.xip.io \
		--param ES_CLUSTER_SIZE=1 \
		--param ES_INSTANCE_RAM=1G \
		--param ES_OPS_INSTANCE_RAM=1G \
		--param PUBLIC_MASTER_URL=https://#{PUBLIC_IP_ADDRESS}:8443
        echo "sleep 40 secounds for create deployer template"
        sleep 40s
        echo "deployer-templates are create"
        echo "start importing images"
        oc import-image logging-auth-proxy:#{EFK_STACK_IMAGEVERSION} \
           --from registry.access.redhat.com/openshift3/logging-auth-proxy:#{EFK_STACK_IMAGEVERSION} --confirm
        oc import-image logging-kibana:#{EFK_STACK_IMAGEVERSION} \
           --from registry.access.redhat.com/openshift3/logging-kibana:#{EFK_STACK_IMAGEVERSION} --confirm
        oc import-image logging-elasticsearch:#{EFK_STACK_IMAGEVERSION} \
           --from registry.access.redhat.com/openshift3/logging-elasticsearch:#{EFK_STACK_IMAGEVERSION} --confirm
        oc import-image logging-fluentd:#{EFK_STACK_IMAGEVERSION} \
     	   --from registry.access.redhat.com/openshift3/logging-fluentd:#{EFK_STACK_IMAGEVERSION} --confirm
    	echo "image import success"    	
    	echo "sleep 40 secounds for create logging-support-template"
        sleep 40s
    	echo "create route and authentication"
    	oc process logging-support-template | oc create -f -
    	echo "right, imagestreams exists, we download this manuell befor"
    	echo "sleep 10 secounds for deploy fluentd image"
    	sleep 10s
    	echo "scale up fluentd"    
    	oc scale dc/logging-fluentd --replicas=1
    	oc project plattform-applikation 	 
    else 
    	echo "Project logging exists, efk-stack work" 
    fi
    SHELL
   
	# Print out final informations about the vm
  	config.vm.provision "shell", name: "Information", run: "always", inline: <<-SHELL
       	oc project plattform-applikation
       	echo **************************************************************************************
       	echo "You can now access the OpenShift console on: https://#{PUBLIC_IP_ADDRESS}:8443/console"
    	echo
    	echo "To use OpenShift oc client, run:"
    	echo "$ vagrant ssh"
    	echo "$ oc login https://#{PUBLIC_IP_ADDRESS}:8443"
    	echo
    	echo "Configured users are (<username>/<password>):"
    	echo "openshift-dev/devel"
    	echo "admin/admin"
    	echo
    	echo "Login now project plattform-applikation"
    	echo
    	echo "If you have the oc client library on your host, you can also login from your host."
    	echo
    	echo "IMPORTANT INFORMATION by first vagrant up, the nexus image is pulling after 5 minutes"
    	echo "After pulling nexus need 2 minutes to get ready"
    	echo "Nexus is running on: http://nexus-nexus.rhel-cdkv2.#{PUBLIC_IP_ADDRESS}.xip.io/nexus"
    	echo
    	echo "Configured user as (<username>/<password>):"
    	echo "admin/admin123"
    	echo 
    	echo "EFK-Stack is installed and running"
    	echo **************************************************************************************
    	echo
  	SHELL

end

